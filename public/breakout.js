import * as keyboard from './keyboard.js';
import * as pointer from './pointer.js';
import { boxCollide } from './physics.js';
const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

pointer.setCanvas(canvas);
pointer.setPause(togglePause);
pointer.setKeyboard(keyboard);

let animFramReq;

let player = {
    x: 10,
    y: 10,
    speed: 1,
    default: {
        width: 120,
        height: 20,
        speed: 10
    },
    width: 120,
    height: 20,
    colour: 'green',
    score: 5,
    pause: true
}

let game = {
    lastTick: window.performance.now(),
    scale: {
        x: 1,
        y: 1
    },
    level: 1
}

let ball = {
    x: 10,
    y: 10,
    velocity: {
        x: 2,
        y: canvas.height * 0.005
    },
    width: 10,
    height: 10,
    colour: 'white'
}

let canvasButtonList = [{
    x: 20,
    y: innerHeight / 3 * 2 + 20,
    width: 100,
    height: 50,
    label: 'PLAY',
    colour: 'brown',
    click: function () {
        togglePause()
    }
}];

let powerUpList = [];

pointer.setHUDButtons(canvasButtonList);

let blockRowList = [];

function ballReset(_ball) {
    _ball.x = player.x + (player.width * 0.5),
        _ball.y = player.y - 10 - _ball.height,
        _ball.velocity = {
            x: 0,
            y: -canvas.height * 0.005
        }
}

function playerRest(_player) {
    player.y = canvas.height - player.height - (canvas.height / 3);
    player.x = (canvas.width * 0.5) - (player.width * 0.5);
    player.width = player.default.width;
    player.speed = player.default.speed;
}

function createBlocks(rows, columns) {
    const stepX = 100 * game.scale.x;
    const stepY = 50 * game.scale.y;
    const width = 100 * game.scale.x;
    const height = 20 * game.scale.y;
    let startX = canvas.width * 0.5 - (rows * width) / 2;
    blockRowList = [];
    for (let j = 0; j < rows; j++) {
        blockRowList[j] = [];
        let startY = stepY;
        for (let i = 0; i < columns; i++) {
            blockRowList[j][i] = {
                x: startX + 10 * j,
                y: startY + 10 * i,
                colour: 'yellow',
                width,
                height
            }
            startY += stepY;
        }
        startX += stepX;
    }
}

function togglePause() {
    game.pause = !game.pause;
    animFramReq = window.requestAnimationFrame(main);
    // buttonPlay.innerText = game.pause ? 'PLAY' : 'PAUSE';
    canvasButtonList[0].label = game.pause ? 'PLAY' : 'PAUSE';
}

document.addEventListener('keydown', (event) => {
    if (keyboard.keyOnce("Escape")) {
        togglePause();
    }
}, false);

function playerMoveLeft(mousePos) {
    console.log(player.speed)
    if (mousePos) {
        let delta = mousePos.x - (player.x + player.width / 2);
        if (delta < player.speed) {
            delta = -player.speed;
        }
        player.x += delta;
    }
    else {
        player.x -= player.speed * game.deltaTime * game.scale.x;
    }
}

function playerMoveRight(mousePos) {
    if (mousePos) {
        let delta = mousePos.x - (player.x + player.width / 2);
        if (delta > player.speed) {
            delta = player.speed;
        }
        player.x += delta;
    }
    else {
        player.x += player.speed * game.deltaTime * game.scale.x;
    }
}

function gameFirstLevel() {
    createBlocks(5, game.level + 1);
    playerRest(player);
    ballReset(ball);
}

function gameNextLevel() {
    game.level++
    player.score++;
    createBlocks(5, game.level + 1);
    playerRest(player);
    ballReset(ball);
    powerUpList = [];
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function createPowerUp(position) {
    const powerIndex = getRandomInt(powerUpLibrary.length);
    const power = powerUpLibrary[powerIndex]
    powerUpList.push({
        x: position.x,
        y: position.y,
        width: 20,
        height: 20,
        colour: power.colour,
        action: power.action
    });
}

const powerUpLibrary = [
    {
        name: 'player.speed',
        action: function () {
            player.speed *= 1.5;
        },
        colour: '#1cff1c'
    },
    {
        name: 'ball.speed',
        action: function () {
            ball.velocity.x *= 1.2;
            ball.velocity.y *= 1.2;
        },
        colour: '#ff1c1c'
    },
    {
        name: 'player.width',
        action: function () {
            player.width *= 1.2;
        },
        colour: '#ffff1c'
    }
];

function update() {
    const mousePos = pointer.getMouse();
    mousePos.width = mousePos.height = 5;
    if (mousePos.x < (player.x + player.width / 2)) {
        playerMoveLeft(mousePos);
    }
    if (mousePos.x > (player.x + player.width / 2)) {
        playerMoveRight(mousePos);
    }

    if (keyboard.keyHeld("ArrowLeft")) {
        playerMoveLeft();
    }
    if (keyboard.keyHeld("ArrowRight")) {
        playerMoveRight();
    }
    if (player.x < 0) {
        player.x = 0;
    }
    if ((player.x + player.width) > canvas.width) {
        player.x = canvas.width - player.width;
    }

    player.y = canvas.height - player.height - (canvas.height / 3);

    ball.x += ball.velocity.x;
    ball.y += ball.velocity.y;

    if (ball.x < 0) {
        ball.x = 0;
        ball.velocity.x = -ball.velocity.x;
    }
    if (ball.x > canvas.width - ball.width) {
        ball.x = canvas.width - ball.width
        ball.velocity.x = -ball.velocity.x;
    }
    if (ball.y < 0) {
        ball.y = 0;
        ball.velocity.y = -ball.velocity.y;
    }
    if (ball.y > canvas.height - ball.height) {
        ball.y = canvas.height - ball.height
        ball.velocity.y = -ball.velocity.y;
    }

    //ball hit block
    let newBlockRowList = [];
    for (let j = 0; j < blockRowList.length; j++) {
        let newBlockColumnList = [];
        for (let i = 0; i < blockRowList[j].length; i++) {
            const currentBlock = blockRowList[j][i];
            if (currentBlock.delete == true) {
                continue;
            }
            newBlockColumnList.push(currentBlock);
            if (boxCollide(currentBlock, ball)) {
                blockRowList[j][i].delete = true;
                ball.velocity.y = -(ball.velocity.y);

                if (Math.random() > 0.5) {
                    createPowerUp(currentBlock);
                }
            }
        }
        if (newBlockColumnList.length > 0) {
            newBlockRowList.push(newBlockColumnList);
        }
    }
    blockRowList = newBlockRowList.slice();

    //update power up
    for (let i = 0; i < powerUpList.length; i++) {
        const currentPowerUp = powerUpList[i];
        if (currentPowerUp.delete) {
            continue;
        }
        currentPowerUp.y += 1.5;

        //player collects power up
        if (boxCollide(currentPowerUp, player)) {
            currentPowerUp.delete = true;
            currentPowerUp.action();
        }

        //remove power up
        if (currentPowerUp.y > player.y) {
            currentPowerUp.delete = true;
        }
    }

    if (blockRowList.length <= 0) {
        gameNextLevel();
    }

    //player hit ball
    if (boxCollide(player, ball)) {
        ball.velocity.y = -Math.abs(ball.velocity.y);

        const ballDelta = (ball.x - player.x) - (player.width * 0.5);
        const newVelocityX = ballDelta / (player.width * 0.25);
        ball.velocity.x = newVelocityX;
    }

    //player misses ball
    if (ball.y > player.y) {
        player.score--;
        ballReset(ball);
    }
}

function drawHUD() {
    ctx.font = '24px sans-serif';

    for (let i = 0; i < canvasButtonList.length; i++) {
        const currentButton = canvasButtonList[i];
        ctx.fillStyle = currentButton.colour;
        ctx.fillRect(currentButton.x, currentButton.y, currentButton.width, currentButton.height);
        ctx.fillStyle = 'white';
        ctx.fillText(currentButton.label, currentButton.x + 20, currentButton.y + currentButton.height / 3 * 2);
    }

    ctx.fillStyle = 'white';
    ctx.fillText(`Score: ${player.score}`, innerWidth - 120, innerHeight / 3 * 2 + 50);

    // draw pointer
    // const mouse = pointer.getMouse();
    // ctx.fillStyle = 'blue';
    // ctx.fillRect(mouse.x, mouse.y, mouse.width, mouse.height);
}

function canvasResize() {
    canvas.width = innerWidth;
    canvas.height = innerHeight;

    const scale = {
        x: canvas.width / 800,
        y: canvas.height / 800
    };

    player.x = 10;
    player.default.width = 120 * scale.x;
    player.default.height = 20 * scale.y;

    player.width = player.default.width;
    player.height = player.default.height;

    game.scale = scale;

    createBlocks(5, game.level + 1);
}

function draw() {

    ctx.clearRect(0, 0, canvas.width, canvas.height);

    for (let j = 0; j < blockRowList.length; j++) {
        for (let i = 0; i < blockRowList[j].length; i++) {
            const currentBlock = blockRowList[j][i];
            if (currentBlock.delete == true) {
                continue;
            }
            ctx.fillStyle = currentBlock.colour;
            ctx.fillRect(currentBlock.x, currentBlock.y, currentBlock.width, currentBlock.height);
        }
    }

    for (let i = 0; i < powerUpList.length; i++) {
        const currentPowerUp = powerUpList[i];
        if (currentPowerUp.delete) {
            continue;
        }
        ctx.fillStyle = currentPowerUp.colour;
        ctx.fillRect(currentPowerUp.x, currentPowerUp.y, currentPowerUp.width, currentPowerUp.height);
    }

    ctx.fillStyle = player.colour;
    ctx.fillRect(player.x, player.y, player.width, player.height);

    ctx.fillStyle = 'black';
    ctx.fillRect(0, player.y + player.height, canvas.width, 2);

    ctx.fillStyle = ball.colour;
    ctx.fillRect(ball.x, ball.y, ball.width, ball.height);

    drawHUD();
}


function main(frames) {
    const deltaTime = frames - game.lastTick;
    game.lastTick = window.performance.now()
    game.deltaTime = deltaTime;

    update();

    draw();

    if (game.pause) {
        window.cancelAnimationFrame(animFramReq);
    }
    else {
        animFramReq = window.requestAnimationFrame(main);
    }
}


export function start() {
    canvasResize();
    gameFirstLevel();
    togglePause();
}

