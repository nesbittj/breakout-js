
let mKeyHeld = {};
let mKeyOnce = {};

document.addEventListener('keydown', (event) => {
    mKeyHeld[event.key] = true;
    if(!mKeyOnce[event.key] || mKeyOnce[event.key].up) {
        mKeyOnce[event.key] = {
            up:false,
            down:true
        }
    }
}, false);

document.addEventListener('keyup', (event) => {
    mKeyHeld[event.key] = false;
    if(!mKeyOnce[event.key]) {
        mKeyOnce[event.key] = {
            up:true,
            down:false
        }
    }
    else {
        mKeyOnce[event.key].up = true;
    }
}, false);

export function keyHeld(keycode) {
    return mKeyHeld[keycode] == true;
}

export function keyOnce(keycode) {
    if(mKeyOnce[keycode] && mKeyOnce[keycode].down == true) {
        mKeyOnce[keycode].down = false;
        return true;
    }
    return false;
}

export function setButtonHeld(keycode,down) {
    mKeyHeld[keycode] = down;
}