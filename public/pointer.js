import { boxCollide } from './physics.js';

// const buttonPlay = document.getElementById('button-play');
// const buttonLeft = document.getElementById('button-left');
// const buttonRight = document.getElementById('button-right');
// buttonPlay.addEventListener('click', function (event) {
//     pause();
// });
// buttonLeft.addEventListener('mousedown', function (event) {
//     keyboard.setButtonHeld("ArrowLeft", true);
// });
// buttonRight.addEventListener('mousedown', function (event) {
//     keyboard.setButtonHeld("ArrowRight", true);
// });
// buttonLeft.addEventListener('touchstart', function (event) {
//     keyboard.setButtonHeld("ArrowLeft", true);
// });
// buttonRight.addEventListener('touchstart', function (event) {
//     keyboard.setButtonHeld("ArrowRight", true);
// });
// buttonLeft.addEventListener('mouseup', function (event) {
//     keyboard.setButtonHeld("ArrowLeft", false);
// });
// buttonRight.addEventListener('mouseup', function (event) {
//     keyboard.setButtonHeld("ArrowRight", false);
// });
// buttonLeft.addEventListener('mouseleave', function (event) {
//     keyboard.setButtonHeld("ArrowLeft", false);
// });
// buttonRight.addEventListener('mouseleave', function (event) {
//     keyboard.setButtonHeld("ArrowRight", false);
// });
// buttonLeft.addEventListener('touchend', function (event) {
//     keyboard.setButtonHeld("ArrowLeft", false);
// });
// buttonRight.addEventListener('touchend', function (event) {
//     keyboard.setButtonHeld("ArrowRight", false);
// });

let canvas, pause, keyboard, hudButtons;
let mouse = {
    x: 0,
    y: 0,
    down: false
}

function updateHUD() {
    for (let i = 0; i < hudButtons.length; i++) {
        const currentButton = hudButtons[i];
        if (boxCollide(currentButton, mouse)) {
            currentButton.click();
        }
    }
}

function dispatchMouseEventFromTouch(event) {
    var touch = event.touches[0];
    var mouseEvent = new MouseEvent("mousemove", {
        clientX: touch.clientX,
        clientY: touch.clientY
    });
    canvas.dispatchEvent(mouseEvent);
}

export function setCanvas(_canvas) {
    canvas = _canvas;

    canvas.addEventListener('mousedown', function (event) {
        mouse.down = true;
        updateHUD();
    }, false);

    canvas.addEventListener('mouseup', function (event) {
        mouse.down = false;
    }, false);

    canvas.addEventListener('mousemove', function (event) {
        var mousePos = _getMousePos(canvas, event);
        mouse.x = mousePos.x;
        mouse.y = mousePos.y;
    }, false);

    canvas.addEventListener('touchstart', function (event) {
        mouse.down = true;
        dispatchMouseEventFromTouch(event);
    }, false);

    canvas.addEventListener('touchend', function (event) {
        mouse.down = false;
        dispatchMouseEventFromTouch(event);
    }, false);

    canvas.addEventListener("touchmove", function (event) {
        dispatchMouseEventFromTouch(event);
    }, false);
}

export function setPause(_pause) {
    pause = _pause;
}

export function setKeyboard(_keyboard) {
    keyboard = _keyboard;
}

export function setHUDButtons(_hudButtons) {
    hudButtons = _hudButtons;
}

function _getMousePos(canvas, event) {
    return {
        x: event.pageX * canvas.width / canvas.offsetWidth - canvas.offsetLeft,
        y: event.pageY * canvas.height / canvas.offsetHeight - canvas.offsetTop
    };
}

export function getMouse() {
    return mouse;
};